#-------------------------------------------------
#
# Project created by QtCreator 2014-06-26T11:25:21
#
#-------------------------------------------------

QT       += core gui
QT       += widgets
TARGET = AlgoritmosEmGrafos
TEMPLATE = app

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x000000

SOURCES += main.cpp\
        mainwindow.cpp \
    graph.cpp \
    edge.cpp \
    vertex.cpp \
    bfs.cpp \
    dfs.cpp \
    topological_order.cpp \
    dijkstra.cpp \
    prim.cpp \
    kruskal.cpp \
    graph_utils.cpp


HEADERS  += mainwindow.h \
    graph.h \
    edge.h \    
    vertex.h \
    list.h \
    bfs.h \
    dfs.h \
    topological_order.h \
    dijkstra.h \
    prim.h \
    kruskal.h \
    graph_utils.h

FORMS    += mainwindow.ui
