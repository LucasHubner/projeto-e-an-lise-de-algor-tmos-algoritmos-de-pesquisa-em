#include "bfs.h"

#include "list.h"

bfs::bfs( Graph *g, QObject *parent ) : QThread(parent) {
    this->g = g;
}

void bfs::run () {
    BFS();
}

void bfs::show () {
    emit update(g);
    sleep(1);
}


// breadth first search
void bfs::BFS() {
    ListVertex *fila= 0;

    Vertex *v, *va;
    Edge *e;
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        v->setFather( NULL );
        v->setD ( INF );
        v->setTo(INF); // mesma struct
        v->setColor ( Qt::white );
    }
    show();

    v = g->getVertexAt(0);
    v->setFather( NULL );
    v->setD ( 0 );
    v->setColor( Qt::gray );
    show();

    ListVertex::append(&fila,v);
    while (fila) {
        v = ListVertex::removeFirst(&fila);
        qDebug() << "BFS: First " << v->getName() << endl;

        for (e=v->getEdges(); e; e = e->getNext()) {
            va = g->getVertexAt(e->getIdAdj());
            qDebug() << "Edge:" << va->getName() << endl;
            if (va->getColor()==Qt::white) {
                va->setFather( v );
                va->setD( v->getD()+1);
                va->setColor( Qt::gray );
                ListVertex::append(&fila, va);
                show();
            }
        }
        v->setColor(Qt::black);
        show();
    }
}
