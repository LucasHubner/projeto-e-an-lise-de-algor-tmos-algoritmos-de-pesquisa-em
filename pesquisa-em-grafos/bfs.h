#ifndef BFS_H
#define BFS_H

#include <QThread>
#include <QColor>

#include "graph.h"

class bfs : public QThread {
    Q_OBJECT

public:
    bfs ( Graph *g, QObject *parent=0 );

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;

    // Busca em profundidade primeiro
    void BFS ();
    void show();
};

#endif // BFS_H
