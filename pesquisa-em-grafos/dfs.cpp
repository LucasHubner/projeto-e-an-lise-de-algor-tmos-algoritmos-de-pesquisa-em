#include "dfs.h"

#include "list.h"

dfs::dfs( Graph *g, QObject *parent ) : QThread(parent) {
    this->g = g;
}

void dfs::run () {
    DFS();
}

void dfs::show () {
    emit update(g);
    sleep(1);
}


void dfs::DFS() {
    ListVertex *fila= 0;

    Vertex *v, *va;
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        v->setFather( NULL );
        v->setD ( INF );
        v->setTo(INF); // mesma struct
        v->setTo( INF );
        v->setColor ( Qt::white );
    }

    show();
    tempo = 0;
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        if(v->getColor() == Qt::white){
            visit(v);
            show();
        }

    }
}

void dfs::visit(Vertex *v){
    tempo++;
    v->setColor(Qt::gray);
    v->setD(tempo);
    show();
    for(Edge *e = v->getEdges(); e; e = e->getNext()){
        Vertex *va = g->getVertexAt(e->getIdAdj());
        if(va->getColor() == Qt::white){
            va->setFather(v);
            show();
            visit(va);
        }
    }
    tempo++;
    v->setColor(Qt::black);
    v->setTo(tempo);

}
