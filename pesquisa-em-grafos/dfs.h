#ifndef DFS_H
#define DFS_H

#include <QThread>
#include <QColor>

#include "graph.h"
#include "vertex.h"
class dfs : public QThread {
    Q_OBJECT

public:
    dfs ( Graph *g, QObject *parent=0 );
    void visit (Vertex *);

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;
     int tempo;

    // Busca em profundidade primeiro
    void DFS ();
    void show();
};

#endif // DFS_H
