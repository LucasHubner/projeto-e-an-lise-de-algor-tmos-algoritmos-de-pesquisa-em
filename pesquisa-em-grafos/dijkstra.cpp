#include "dijkstra.h"

dijkstra::dijkstra( Graph *g, QString origem, QObject *parent) : QThread(parent) {
    this->g = g;
    this->startVertex = origem.toAscii();
}

void dijkstra::run () {
    DIJKSTRA();
}

void dijkstra::show () {
    emit update(g);
    sleep(1);
}


void dijkstra::DIJKSTRA() {
    ListVertex *Q = 0;
    Vertex *v;
    Vertex *s = g->getVertexAt(g->getVertexIndex(startVertex));

    init(&Q,s);
    show();
    while(Q){
    v = ListVertex::removeLeast(&Q);
    v->setColor(Qt::gray);
    show();
        for(Edge *e = v->getEdges(); e; e = e->getNext()){
            Vertex *va = g->getVertexAt(e->getIdAdj());
            if(va->getD() > (v->getD() + e->getW())){
                va->setFather(v);
                va->setD(v->getD() + e->getW());
            }
        }
        v->setColor(Qt::black);
        show();
    }
}

void dijkstra::init(ListVertex **Q, Vertex *s){
    Vertex *v;
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        v->setColor(Qt::white);
        v->setFather(0);
        v->setD(INF);
        ListVertex::append(Q,v);
        show();
    }
    s->setD(0);
}
