#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <QThread>
#include <QColor>

#include "vertex.h"
#include "graph.h"
#include "graph_utils.h"

class dijkstra : public QThread {
    Q_OBJECT

public:
    dijkstra ( Graph *g, QString origem, QObject *parent = 0);

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;
    QString startVertex;
    void init(ListVertex **,Vertex *);
    void DIJKSTRA();
    void show();
};
#endif // DIJKSTRA_H
