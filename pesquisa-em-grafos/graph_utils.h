#ifndef GRAPH_UTILS_H
#define GRAPH_UTILS_H

#include "vertex.h"
#include "list.h"

class graph_utils
{
public:
    graph_utils();

    Vertex* extraiMin(ListVertex**);

    void Union(Vertex*, Vertex*);

};

#endif // GRAPH_UTILS_H
