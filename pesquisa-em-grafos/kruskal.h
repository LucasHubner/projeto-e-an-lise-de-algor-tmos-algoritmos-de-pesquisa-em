#ifndef KRUSKAL_H
#define KRUSKAL_H

#include <QThread>
#include <QColor>

#include "mainwindow.h"
#include "vertex.h"
#include "list.h"
#include "graph.h"
#include "mainwindow.h"

class kruskal : public QThread {
    Q_OBJECT

public:
    kruskal ( Graph *g,  QObject *parent=0 );

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;
    MainWindow *window;

    void KRUSKAL();//Edge *
    void show();
};

#endif // KRUSKAL_H
