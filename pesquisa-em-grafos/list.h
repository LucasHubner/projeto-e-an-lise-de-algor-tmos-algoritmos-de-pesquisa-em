#ifndef LIST_H
#define LIST_H

#include "vertex.h"

class ListVertex {
protected:
    Vertex *v;
    ListVertex *next;
public:
    ListVertex ( Vertex *v, ListVertex *next ) {
        this->v = v;
        this->next = next;
    }

    int getD ( ) {
        return this->v->getD();
    }

    static void insertBefore( ListVertex **first, Vertex *v ) {
        ListVertex *n = new ListVertex ( v, *first );
        *first = n;
    }

    static void append ( ListVertex **e, Vertex *v ) {
        if (*e)
            append( &(*e)->next, v );
        else
            *e = new ListVertex ( v, 0 );
    }

    static Vertex* removeFirst ( ListVertex **e, Vertex *v=0 ) {
        if (*e) {
            v = (*e)->v;

            ListVertex *aux= *e;
            *e = aux->next;
            aux->next = 0;
            delete aux;
        }
        return v;
    }

    static Vertex* removeLeast ( ListVertex **e, Vertex *v=0 ) {
        if (*e) {
            if (!v  || ((*e)->getD()<v->getD())) v = (*e)->v;
            v = removeLeast( &(*e)->next,v);


            if (v==(*e)->v) {
                ListVertex *aux = *e;
                *e = aux->next;
                aux->next = 0;
                delete aux;
            }
        }
        return v;
    }

    QString getList ( ) {
        QString s= v->getName() + ";";
        if (next)
            s += next->getList();

        return s;
    }

    ~ListVertex () {
        if (next) delete next;
    }
};

#endif // LIST_H
