#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include "bfs.h"
#include "dfs.h"
#include "topological_order.h"
#include "dijkstra.h"
#include "prim.h"
#//include "kruskal.h"


#define DFS           0
#define BFS           1
#define ORD_TOPOLOG   2
#define DIJKSTRA      3
#define PRIM          4
#define KRUSKAL       5
#define FORDFULKERSON 6

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this);

    this->ui->statusBar->addWidget( this->ui->labelInicial );
    this->ui->statusBar->addWidget( this->ui->cbOrigem );
    this->ui->statusBar->addWidget( this->ui->labelFinal  );
    this->ui->statusBar->addWidget( this->ui->cbFinal );
    this->ui->statusBar->addWidget( this->ui->textEdit );
    this->ui->statusBar->addWidget( this->ui->pushButton );

    QMainWindow::paintEvent(new QPaintEvent(this->geometry()));

    this->alg   = NULL;
    this->graph = NULL;

    newGraph();

    idmostrar=0;
}

void MainWindow::newGraph ( ) {
    if (graph) delete graph;
    this->graph=new Graph(0, this);
    connect( this, SIGNAL (mostrar( Graph* )), this, SLOT ( mostrarGrafo(Graph* ) ) );
    this->tmp=NULL;
}

void MainWindow::runAlgoritm ( int i ) {
    if (alg) delete alg;
    switch (i) {
        case DFS: this->alg = new dfs ( graph, this ); break;
        case BFS: this->alg = new bfs ( graph, this ); break;
        case ORD_TOPOLOG: this->alg = new topological_order ( graph, this ); break;
        case DIJKSTRA : this->alg = new dijkstra ( graph, ui->cbOrigem->currentText()); break;
        //case KRUSKAL : this->alg = new kruskal ( graph, this, this ); break;
        case PRIM : this->alg = new prim ( graph, ui->cbOrigem->currentText() ); break;
    }
    connect( alg,  SIGNAL ( update(Graph*)), this, SLOT ( mostrarGrafo(Graph* ) ) );
    connect( alg, SIGNAL( finished()) , this, SLOT( finished()) );
    alg->start();
}
void MainWindow::finished() {
    //if (dynamic_cast<topological_order*>(alg))
    //    ui->textEdit->setText( ((topological_order*)alg)->getOrder() );
}

void MainWindow::paintEvent(QPaintEvent *) {
    graph->paint();
    if (!this->tmp) return;
    tmp->paint();
}

void MainWindow::mostrarGrafo ( Graph *g ) {
    this->tmp = g;
    update ();
}

void MainWindow::on_actionLoad_triggered() {
    QDir::setCurrent("../files");
    qDebug() << QDir::currentPath();
    QString filename =  QFileDialog::getOpenFileName( this, tr("Open Document"),
                                                      QDir::currentPath(),
                                                      tr("Document files (*.txt);All files (*.*)"), 0,
                                                      QFileDialog::DontUseNativeDialog );
    if( !filename.isNull() ) {
        qDebug() << filename.toAscii();
        newGraph();
        QString s = graph->loadFromFile(filename);
        ui->cbOrigem->addItems( s.split(";"));
        ui->cbFinal->addItems( s.split( ";") );
        emit mostrar ( graph );
    }
}

void MainWindow::on_pushButton_clicked() {
    if (this->graph!=NULL)
        this->ui->textEdit->setText(
                this->graph->getPath( this->ui->cbFinal->currentText() ));
}

void MainWindow::on_actionBusca_em_profundidade_triggered() {
    if (this->graph!=NULL) runAlgoritm(DFS);
}

void MainWindow::on_actionBusca_em_largura_triggered() {
   if (this->graph!=NULL) runAlgoritm( BFS );
}

void MainWindow::on_actionDijkstra_triggered() {
   if (this->graph!=NULL) runAlgoritm( DIJKSTRA );
}

void MainWindow::on_actionOrdena_o_Topol_gica_triggered() {
     if (this->graph!=NULL) runAlgoritm( ORD_TOPOLOG );
}

void MainWindow::on_actionKRUSKAL_triggered() {
     if (this->graph!=NULL) runAlgoritm( KRUSKAL );
}

void MainWindow::on_actionPRIM_triggered(){
    if (this->graph!=NULL) runAlgoritm( PRIM );
}

MainWindow::~MainWindow() {
    if (graph) delete graph;
    if (alg) delete alg;
    delete ui;
}

