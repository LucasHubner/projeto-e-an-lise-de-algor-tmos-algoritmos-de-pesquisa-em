#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "ui_mainwindow.h"
#include "graph.h"


#include <QPainter>
#include <QPaintEvent>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

signals:
    void mostrar ( Graph *);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void finished();
    void mostrarGrafo ( Graph * );

private slots:
    void on_actionLoad_triggered();

    void on_actionBusca_em_profundidade_triggered();

    void on_pushButton_clicked();

    void on_actionBusca_em_largura_triggered();

    void on_actionDijkstra_triggered();

    void on_actionOrdena_o_Topol_gica_triggered();

    void on_actionKRUSKAL_triggered();

    void on_actionPRIM_triggered();

private:
    Ui::MainWindow *ui;
    Graph *graph, *tmp;
    QThread *alg;
    int idmostrar;

    void runAlgoritm ( int i );
    void newGraph ();

protected:
    virtual void paintEvent(QPaintEvent *);
};


#endif // MAINWINDOW_H
