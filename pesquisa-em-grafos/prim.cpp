#include "prim.h"

prim::prim( Graph *g,QString origin, QObject *parent ) : QThread(parent) {
    this->g = g;
    this->origin = origin;
}

void prim::run () {
    PRIM();
}

void prim::show () {
    emit update(g);
    sleep(1);
}

void prim::PRIM() {
    ListVertex *fila= 0;
    Vertex *v, *va, *s;
    s = g->getVertexAt(g->getVertexIndex(this->origin));

    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        v->setFather( NULL );
        v->setD ( INF );
        v->setColor ( Qt::white );
        ListVertex::append(&fila,v);
    }
    show();
    s->setD(0);
    while(fila){
        v = extraiMin(&fila);
        v->setColor(Qt::gray);
        show();
        for (Edge* e=v->getEdges(); e; e=e->getNext()) {
            va = g->getVertexAt(e->getIdAdj());
            if((va->getColor() == Qt::white) && (e->getW() < va->getD())){
                va->setFather(v);
                va->setD(e->getW());
            }
        }
        v->setColor(Qt::black);
        show();
    }
}
