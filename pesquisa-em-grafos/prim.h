#ifndef PRIM_H
#define PRIM_H

#include <QThread>
#include <QColor>

#include "graph.h"
#include "graph_utils.h"

class prim : public QThread , graph_utils {
    Q_OBJECT

public:
    prim ( Graph *g, QString origin, QObject *parent=0 );

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;
    QString origin;

    void PRIM ();
    void show();


};

#endif // PRIM_H
