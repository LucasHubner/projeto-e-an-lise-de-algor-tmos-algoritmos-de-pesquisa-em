#include "vertex.h"
#include "list.h"
#include "edge.h"
#include "list.h"
#include "topological_order.h"


topological_order::topological_order( Graph *g, QObject *parent ) : QThread(parent) {
    this->g = g;
}

void topological_order::run () {
    ORDTOP();
}

void topological_order::show () {
    emit update(g);
    sleep(1);
}


ListVertex* topological_order::ORDTOP() {
    ListVertex *fila= 0;

    Vertex *v, *va;
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        v->setFather( NULL );
        v->setD ( INF );                      v->setTo(INF); // mesma struct
        v->setColor ( Qt::white );
    }
    show();
    tempo = 0;
    show();
    for (int i=0; i<g->getVertexCount(); i++) {
        v = g->getVertexAt(i);
        if(v->getColor() == Qt::white){
            visit(v,fila);

        }
        show();

    }
    return fila;
}

void topological_order::visit(Vertex * v ,ListVertex* fila){
    tempo++;
    v->setColor(Qt::gray);
    v->setD(tempo);
    show();
    for(Edge *e = v->getEdges(); e; e = e->getNext()){
        Vertex *va = g->getVertexAt(e->getIdAdj());
        if(va->getColor() == Qt::white){
            va->setFather(v);
            show();
            visit(va, fila  );
        }
    }
    tempo++;
    v->setColor(Qt::black);
    v->setTo(tempo);
    ListVertex::append(&fila, v);

}
