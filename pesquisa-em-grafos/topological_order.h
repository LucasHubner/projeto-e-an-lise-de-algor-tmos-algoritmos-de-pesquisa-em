#ifndef TOPOLOGICAL_ORDER_H
#define TOPOLOGICAL_ORDER_H

#include <QThread>
#include <QColor>

#include "graph.h"
#include "list.h"
#include "vertex.h"

class topological_order : public QThread {

    Q_OBJECT
public:
    topological_order( Graph *g, QObject *parent=0 );

protected:
    void run();

signals:
    void update ( Graph * );

private:
    Graph *g;
    int tempo;

    void visit (Vertex *, ListVertex*);
    ListVertex* ORDTOP ();
    void show();
};

#endif // TOPOLOGICAL_ORDER_H
